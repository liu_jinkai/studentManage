/*
 * @Author: 刘金开 liujinkai@jiankeyan.com
 * @Date: 2023-07-10 18:04:36
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-08-07 11:05:41
 * @FilePath: \font\src\services\api\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import axios from 'axios';

const baseURL = process.env.NODE_ENV === 'development' ? '/api' : 'https://hotpot.ren:44333/api';
const api = axios.create({
    baseURL: baseURL
});

export default api;
