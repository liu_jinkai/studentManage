/*
 * @Description: 
 * @Version: 2.0
 * @Author: , // 文件作者
 * @Date: 2023-07-10 18:04:36
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-07-21 22:13:51
 */
// router.js
import { createRouter, createWebHistory,createWebHashHistory } from 'vue-router';
import login from "./view/login.vue";
import home from "./view/home.vue";
import sys from "./view/sys.vue";
import classis from './view/class.vue';
import student from './view/student.vue';
import course from './view/course.vue';
import {useStore  } from "vuex";

const routes = [
    { path: '/', component: login,meta: { requiresAuth: false } },
    {
        path: '/home',
        component: home,
        redirect: '/student_data', // Add this redirect property
        meta: { requiresAuth: true },
        children: [
            { path: '/employee', component: sys,meta: { requiresAuth: true }},
            { path: '/role', component: sys,meta: { requiresAuth: true } },
            { path: '/speciality', component: sys,meta: { requiresAuth: true } },
            { path: '/class', component: classis,meta: { requiresAuth: true } },
            { path: '/student_data', component: student,meta: { requiresAuth: true } },
            { path: '/course', component: course,meta: { requiresAuth: true } },
        ]
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to, from, next) => {
    const store = useStore();
    const isAuthenticated = store.state.isAuthenticated;

    if (to.meta.requiresAuth && !isAuthenticated) {
        // 需要身份验证但用户未登录，重定向到登录页面
        next('/');
    } else {
        // 其他情况，允许导航
        next();
    }
});


export default router;
