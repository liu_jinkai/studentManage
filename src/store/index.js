/*
 * @Author: 刘金开 liujinkai@jiankeyan.com
 * @Date: 2023-07-11 15:50:12
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-07-24 17:56:07
 * @FilePath: \font\src\store\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// store/index.js
import { createStore } from "vuex";

import createPersistedstate from 'vuex-persistedstate'
const store = createStore({
  state() {
    return {
      isAuthenticated: false,
    };
  },
  mutations: {
    login(state) {
      state.isAuthenticated = true;
    },
    exit(state) {
      state.isAuthenticated = false;
    },
  },
  plugins: [
    createPersistedstate({
      storage:localStorage, // 使用sessionStorage进行持久化存储
      key:'isAuthenticated'
    }),
  ],
});

export default store;
