/*
 * @Author: 刘金开 liujinkai@jiankeyan.com
 * @Date: 2023-07-17 23:03:31
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-07-19 00:00:22
 * @FilePath: \font\src\services\api\class.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import api from "./index.js";

export function getClass(query){
    return api.get(`/class/?page=${query.page}&pageSize=${query.pageSize}&specialityID=${query.specialityID}&name=${query.name}`)
}
export function editClass(data){
    return api.put("/class/",data)
}
export function createClass(data){
    return api.post("/class/",data)
}
export function deleteClass(ids){
    return api.delete("/class/",{data:ids})
}