import api from "./index.js";

export function getCaptchaApi(){
    return api.get("/getRandom")
}

export function postLogin(data){
    return api.post("/login",data)
}

export function getAutoLogin(){
    return api.get("/sys/autoLogin")
}

export function getExit(){
    return api.get("/sys/exit")
}
