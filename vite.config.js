/*
 * @Author: 刘金开 liujinkai@jiankeyan.com
 * @Date: 2023-07-10 18:04:36
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-07-24 11:43:01
 * @FilePath: \font\vite.config.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
      vue()
  ],
  server: { //主要是加上这段代码
    host: '127.0.0.1',
    port: 8080,
    proxy: {
      '/api': {
        target: 'https://114.115.217.17:8000',	//实际请求地址
        changeOrigin: true,
        withCredentials: true,
        secure: false,
        rewrite: (path) => path.replace(/^\/api/, '')
      },
    }
  }
})
