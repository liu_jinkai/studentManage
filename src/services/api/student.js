/*
 * @Author: 刘金开 liujinkai@jiankeyan.com
 * @Date: 2023-07-17 23:03:31
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-07-20 00:11:43
 * @FilePath: \font\src\services\api\class.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import api from "./index.js";

export function getStudent(query){
    return api.get(`/studentData/?page=${query.page}&pageSize=${query.pageSize}`)
}
export function getStudentAll(query){
    return api.get(`/studentData/?page=${query.page}&pageSize=${query.pageSize}&name=${query.name}`)
}
export function editStudent(data){
    return api.put("/studentData/",data)
}
export function createStudent(data){
    return api.post("/studentData/",data)
}
export function deleteStudent(ids){
    return api.delete("/studentData/",{data:ids})
}