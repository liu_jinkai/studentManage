/*
 * @Author: 刘金开 liujinkai@jiankeyan.com
 * @Date: 2023-07-20 15:41:00
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-07-20 15:49:46
 * @FilePath: \font\src\services\api\course.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import api from "./index.js";

export function getCourse(query){
    return api.get(`/course//?page=${query.page}&pageSize=${query.pageSize}&studentDataID=${query.studentDataID}&classID=${query.classID}`)
}
export function editCourse(data){
    return api.put("/course//",data)
}
export function createCourse(data){
    return api.post("/course//",data)
}
export function deleteCourse(ids){
    return api.delete("/course//",{data:ids})
}