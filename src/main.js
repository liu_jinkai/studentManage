/*
 * @Author: 刘金开 liujinkai@jiankeyan.com
 * @Date: 2023-07-10 18:04:36
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-07-24 17:50:21
 * @FilePath: \font\src\main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import router from './router';
import api from './services/api';
import store from './store';


const app = createApp(App);
// 全局注册 Vue Router
app.use(router);
app.use(store)
// 将 Axios 实例挂载到全局属性
app.config.globalProperties.$http = api;
app.use(ElementPlus);
app.mount('#app');
