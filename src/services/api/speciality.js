/*
 * @Author: 刘金开 liujinkai@jiankeyan.com
 * @Date: 2023-07-11 20:50:27
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-07-18 22:18:20
 * @FilePath: \font\src\services\api\speciality.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import api from "./index.js";

export function getSpecialAll(query){
    return api.get(`/speciality/?page=${query.page}&pageSize=${query.pageSize}`)
}
export function getSpecialOne(query){
    return api.get(`/speciality/?page=${query.page}&pageSize=${query.pageSize}&name=${query.name}`)
}
export function addSpecial(data){
    return api.post("/speciality/",data)
}
export function editSpecial(data){
    return api.put("/speciality/",data)
}
export function deleteSpecial(ids){
    return api.delete("/speciality/",{data:ids})
}