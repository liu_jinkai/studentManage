/*
 * @Author: 刘金开 liujinkai@jiankeyan.com
 * @Date: 2023-07-20 10:18:22
 * @LastEditors: 刘金开 liujinkai@jiankeyan.com
 * @LastEditTime: 2023-07-20 11:03:12
 * @FilePath: \font\src\utils\exportExcel.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 导出下载
import FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import {getCurrentDate} from './yearMonthDay'
export function  exportDownload(val){
// val为导出的表格名称

const { year, month, day } = getCurrentDate();


//#tableId为我们表格的ID
    let dom = document.querySelector('#tableId');
    let xlsxName = `${val}导出表格${year}-${month}-${day}.xlsx`;
//导出的内容只做解析，不进行格式转换
    let xlsxParam = { raw: true }; 
    let wb = XLSX.utils.table_to_book( dom, xlsxParam );
 //下面代码阐明了保存的类型为什么
    let wbout = XLSX.write(wb, {
        bookType: 'xlsx',
        bookSST: true,
        type: 'array',
    });
    try {
        FileSaver.saveAs(new Blob([wbout], { type: 'application/octet-stream' }),xlsxName);
    }catch(e){
        if (typeof console !== 'undefined') {
          ElNotification({
                                      title: 'Error',
                                      message: `导出信息失败`,
                                      type: 'error',
                          })
        }
    }
    return wbout;


      }