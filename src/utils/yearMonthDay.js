export  function getCurrentDate() {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();
  
    return {
      year: currentYear,
      month: currentMonth,
      day: currentDay
    };
  }
  
  // 使用示例：
  
  